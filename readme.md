# R-peptidomics

# Description
This script is designed to help in the statistical analysis of peptidomics quantitative data. It is an R script to manipulate peptide quantification data from MassChroQ or other peptide quantification software.
Among others, this script helps in generating figures such as Venn Diagrams, Principal Components Analysis, Boxplot, Heatmaps, Clustering, and Protein Mapping.
Example figures below are produced from 5 technical replicates of the gastric and intestinal digestion kinetics (0.1min to 120min) of a skimmed milk powder.

**Principal Components Analysis:**

 ![PCA](https://forgemia.inra.fr/julien.jardin/r-peptidomics/-/raw/master/ouput_example/PCA_axes_1-2.png?ref_type=heads)

**Venn Diagram:**

![VENN](https://forgemia.inra.fr/julien.jardin/r-peptidomics/-/raw/master/ouput_example/Venn_SMP_gastric.png?ref_type=heads)

**Boxplot:**

![BOXPLOT](https://forgemia.inra.fr/julien.jardin/r-peptidomics/-/raw/master/ouput_example/boxplot_mw.png?ref_type=heads)

**Heatmap:**

![HEATMAP](https://forgemia.inra.fr/julien.jardin/r-peptidomics/-/raw/master/ouput_example/Heatmap_scaled.png?ref_type=heads)

**Peptide Clusters Visualisation:**

 ![CLUSTERS](https://forgemia.inra.fr/julien.jardin/r-peptidomics/-/raw/master/ouput_example/Figure_median_clusters.png?ref_type=heads)

**Peptide Clusters Analysis (CATDES):**

![CATDES](https://forgemia.inra.fr/julien.jardin/r-peptidomics/-/raw/master/ouput_example/Cluster_CATDES.png?ref_type=heads)

**Pie Charts:**

![CHARTS](https://forgemia.inra.fr/julien.jardin/r-peptidomics/-/raw/master/ouput_example/pie_chart_g.png?ref_type=heads)

**Protein Mapping:**

![MAPPING](https://forgemia.inra.fr/julien.jardin/r-peptidomics/-/raw/master/ouput_example/Protein_mapping_a1_BCN.png?ref_type=heads)


# Download and install
All downloadable files and example data concerning r-peptidomics can be found at [ForgeMIA](https://forgemia.inra.fr/julien.jardin/r-peptidomics).
No installation is required, simply open the r-peptidomics.Rmd R markdown script into Rstudio and run through it adapting the script to your data. Use files in 'example_data' directory to visualize what the script can perform.

## Authors and acknowledgements
This project is the work of Julien Jardin (contact: julien.jardin@inrae.fr). 
Data example was produced at [UMR STLO](https://stlo.rennes.hub.inrae.fr/) (65 rue de Saint Brieuc, 35042 Rennes, FRANCE) on a Qexactive mass spectrometer coupled to reverse phase NanoLC. Digestion samples were prepared according to the INFOGEST static digestion protocol (Minekus et al., 2014) from commercial skimmed milk powder at Agroscope (Schwarzenburgstrasse 161, 3003 Bern, Switzerland) and sent to STLO freeze dried as part of a peptidomics ring-trial accross 10 different laboratories. More details and results from this ring trial can be found in [Portmann et al., Food research International, 2023](https://hal.inrae.fr/hal-04094987)

# Contributing
You are welcome to contribute, copy, modify, fork if you need it, respecting the terms of the licence.

# Licence
This work is licensed under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). Refer to this page when citing this work: https://forgemia.inra.fr/julien.jardin/r-peptidomics

# Project status
This project is still under development and may be updated in the future

